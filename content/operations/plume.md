---
title: "Plume"
description: "Plume"
date: 2022-12-22
dateCreated: 2022-12-22
weight: 11
extra:
  parent: 'operations/debogage.md'
---

## Bug and debug

If you can't follow a new user and have this error:

```
2022-04-23T19:26:12.639285Z  WARN plume::routes::errors: Db(DatabaseError(UniqueViolation, "duplicate key value violates unique constraint \"follows_unique_ap_url\""))
```

You might have an empty field in your database:

```
plume=> select * from follows where ap_url='';
  id  | follower_id | following_id | ap_url
------+-------------+--------------+--------
 2118 |          20 |          238 |
(1 row)
```

Simply set the `ap_url` as follows:

```
plume=> update follows set ap_url='https://plume.deuxfleurs.fr/follows/2118' where id=2118;
UPDATE 1
```
