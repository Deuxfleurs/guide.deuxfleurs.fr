---
title: "Pannes"
description: "Pannes"
weight: 70
sort_by: "weight"
extra:
  parent: 'operations/_index.md'
---

Nous aimerions mettre en place une culture du postmortem.
C'est très important pour s'améliorer et apprendre de ses erreurs,
pour prendre le temps de se questionner sur ce qui a disfonctionné,
et réfléchir à comment changer en profondeur nos pratiques.
Et puis, plus simplement, si le problème venait à arriver 
de nouveau, nous aurions alors déjà documenté comment le résoudre !
