---
title: "SSH"
description: "SSH"
weight: 100
extra:
  parent: 'operations/acces.md'
---

SSH permet de se connecter aux machines du cluster à administrer (`staging` ou `prod`).

# Ajout d'une nouvelle clé SSH au cluster

Dans le dépot [nixcfg](https://git.deuxfleurs.fr/deuxfleurs/nixcfg), éditer le
fichier `cluster/CLUSTER/cluster.nix`, où `CLUSTER` est à remplacer par `prod`
ou `staging`.

La variable qui nous intéresse est `deuxfleurs.adminAccounts`. On trouve une
définition de la forme suivante :

```nix
  deuxfleurs.adminAccounts = {
    lx = [
      "ssh-ed25519 ...."
    ];
    quentin = [
      "ssh-rsa ...."
      "ssh-rsa ...."
    ];
    ...
  };
```

Ici, `lx` et `quentin` correspondent à des noms d'utilisateur linux sur les
machines du cluster, et les `"ssh-ed25519 ..` ou `"ssh-rsa ..."` sont les clefs
SSH publiques qui permettent de se connecter à ces utilisateurs.

Ajouter un attribut à `deuxfleurs.adminAccounts` avec le nom d'utilisateur et sa
clef ssh publique choisie. Par exemple, pour ajouter une nouvelle utilisatrice
`alice` :

```nix
  deuxfleurs.adminAccounts = {
    lx = [
      "ssh-ed25519 ...."
    ];
    quentin = [
      "ssh-rsa ...."
      "ssh-rsa ...."
    ];
    ...
    alice = [
      "clef SSH publique d'alice"
    ];
```

Commiter et pousser ces modifications (`git commit/push`). 

Un autre administrateur doit ensuite déployer ces modifications sur les machines
du cluster en utilisant le script `./deploy_nixos` du dépot `nixcfg`.

*TODO*: dire ce qu'il faut mettre dans le `.ssh/config`.

Vous pouvez ensuite tester de vous connecter à une machine du cluster :

```bash
ssh caribou # staging
# ou
ssh pasteque # prod
```


Pour terminer la création d'un nouveau compte utilisateur linux sur le cluster,
la dernière étape est de lui [assigner un mot de
passe](@/operations/user_passwd.md).
