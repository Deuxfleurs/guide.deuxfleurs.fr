---
title: "Mot de passe unix"
description: "Mot de passe unix"
weight: 200
extra:
  parent: 'operations/acces.md'
---

The last step of adding an administrator is to select a user password for
yourself on the cluster you are now in charge of (`prod` or `staging`).

In the [nixcfg](https://git.deuxfleurs.fr/deuxfleurs/nixcfg) repository,
use the passwd utility to set your shell password:

```
./passwd
> Usage: ./passwd <cluster name> <username>
> The cluster name must be the name of a subdirectory of cluster/
```

This commited changes to Deuxfleurs' password store, do verify your modifications before pushing them:

```
cd ~/.password-store/deuxfleurs
git diff
git push
```

These changes must be deployed to the machines to take effect. Ask another
administrator to deploy them. They will need to use the script
`./deploy_passwords` from the `nixcfg` repository after pulling your changes.
