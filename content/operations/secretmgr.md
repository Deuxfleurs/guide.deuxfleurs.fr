---
title: "Gestion des secrets"
description: "Gestion des secrets pour les applications"
date: 2025-02-25
dateCreated: 2025-02-25
weight: 11
extra:
  parent: 'operations/deployer.md'
---

Cette page documente l'usage du script `secretmgr` dans le dépôt nixcfg.

- écrire un fichier `cluster/prod/app/<monapplication>/secrets.toml` qui décrit les secrets nécessaires à l'application
- TODO expliquer les différents types de secrets disponibles
- établir un tunnel SSH et lancer le script `tlsproxy` pour avoir accès à LDAP et Consul (voir `doc/onboarding.md` pour la conf SSH recommandée)
- utiliser `secretmgr` pour générer les secrets, les copier dans Consul, et créer un utilisateur de service dans LDAP
- TODO mot de passe admin LDAP

Attention, le script nécessite Nix (sinon, il faut installer les dépendances Python soi-même)
