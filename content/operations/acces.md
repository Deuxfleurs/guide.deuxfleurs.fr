---
title: "Accès"
description: "Accès"
sort_by: "weight"
weight: 5
extra:
  parent: 'operations/_index.md'
---

Ici l'on traite de comment gagner accès au cluster de Deuxfleurs, quand on a reçu la _terrible responsabilité_ de sysadmin. Vous êtes prêt⋅e ? Les étapes sont les suivantes :
- [le dépôt des secrets](@/operations/pass.md)
- [SSH](@/operations/ssh.md)
- [mot de passe linux](@/operations/user_passwd.md)
