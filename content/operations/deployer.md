---
title: "Déployer du logiciel"
description: "Déploiement d'un logiciel"
sort_by: "weight"
date: 2022-12-22
weight: 30
extra:
  parent: 'operations/_index.md'
---


# Empaqueter

Packager avec nix un conteneur Docker, le publier.

# Secrets

Créer les secrets avec [secretmgr](@/operations/secretmgr.md)

# Base de données SQL

Si besoin, [créer une base de données PostgreSQL](@/operations/create_database.md)

# Service Nomad

Créer un service Nomad

Voir les différentes déclarations dans [nixcfg cluster/prod](https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/cluster/prod/app) :
  - `core/deploy/diplonat.hcl`
  - `core/deploy/tricot.hcl`
  - `guichet/deploy/guichet.hcl`

Voir la [documentation nixcfg](https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/doc/architecture.md#deploying-stuff-on-nomad) pour utiliser la ligne de commande Nomad.

# Sauvegardes

Voir la section appropriée : [Sauvegardes](@/operations/sauvegardes.md)

# Supervision

Voir la section appropriée : [Supervision](@/operations/supervision.md)


