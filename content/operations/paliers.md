---
title: "Historique des paliers"
description: "Historique des passages de paliers"
weight: 80
sort_by: "weight"
extra:
  parent: 'operations/_index.md'
---

Cette page sert à entretenir un historique des passages de paliers que nous réalisons dans le cadre de la modération des activités sur l'infrastructure de Deuxfleurs. La méthodologie de celle-ci, décrite dans nos [conditions générales d'utilisation](@/vie_associative/cgu.md), a été définie début mars 2024, lors de notre premier incident en la matière.

## Samedi 22 juin 2024
* Passage au palier 1 après avoir constaté beaucoup de connexions durant la nuit sur Jitsi (entre minuit et 5h du matin), sur les dernières semaines.
* Passage au palier 2 après avoir constaté des noms de salon Jitsi présents lors du dernier incident (c'est-à-dire le premier de notre association).
* On met en place au niveau HTTP le rejet des connexions sur les salons concernés. On met en place sur Jitsi un message d'avertissement rappelant notre détermination à lutter contre les mésusages de nos services. Nous constatons que notre service Jitsi est le 15ème résultat obtenu quand on type « Jitsi » sur Google avec une IP française, le 5ème qui correspond à une instance Jitsi. On modifie donc notre `robots.txt` pour que Jitsi ne soit plus indexé.