---
title: "Supervision"
description: "Supervision"
weight: 58
sort_by: "weight"
extra:
  parent: 'operations/_index.md'
---

# Journaux

Les journaux ne sont pas centralisés aujourd'hui.
Vous pouvez les consulter avec `docker logs`, `nomad` et `journalctl`.

# Métriques

Grafana est accessible à l'adresse suivante : https://grafana.deuxfleurs.fr

La connexion est possible avec ses identifiants Guichet (via LDAP).

Pour les admins, il est aussi possible d'utiliser le mot de passe admin en allant le chercher dans Consul KV.

Les dashboards ne sont pour l'instant pas stockés dans un dépot git, ils sont édités manuellement dans l'interface de Grafana.

Il y a également une instance Grafana de staging, sans intégration LDAP/Guichet : https://grafana.staging.deuxfleurs.org

# Supervision et alerting externe

Nous avons un système de supervision externe, accessible à l'adresse <https://status.deuxfleurs.fr>.
Il s'agit d'une instance de [Uptime Kuma](https://github.com/louislam/uptime-kuma), hébergée gracieusement par [RésiLien](https://resilien.fr/).

Son but est de vérifier le bon fonctionnement des services exposés publiquement par Deuxfleurs : sites web statiques, services web (cryptpad, jitsi, plume), email, ainsi que l'API S3 de Garage.

Pour rajouter des services à surveiller ou configurer des envois d'alertes, les identifiants de connexion sont dans le [dépôt des secrets](@/operations/pass.md).

Un bot envoie les alertes sur le canal Matrix `deuxfleurs::alertes`. Ce bot est un compte Matrix chez Grésille, pour pouvoir envoyer des alertes même si l'infrastructure Matrix de Deuxfleurs est en panne. Les identifiants de ce compte sont dans le [dépôt des secrets](@/operations/pass.md) (entrée `prod/botmonitoringdeuxfleurs`).

# Alerting interne

Nous ne disposons actuellement pas de supervision interne complète avec envoi d'alertes.

Une telle supervision interne serait complémentaire à la supervision externe : elle permettrait de détecter des problèmes en amont qui ne sont pas forcément encore visibles sur les services.
Par exemple, Garage tolère la panne d'une zone sans impacter le service, il est donc facile de ne pas se rendre compte de la panne ... jusqu'à ce qu'une deuxième panne arrive !

Les éléments suivants seraient pertinents à surveiller :

- système : espace disque, état SMART des disques, charge I/O
- connectivité : connectivité interne Wireguard, IPv6
- état du cluster Garage (perte d'une zone ou d'un noeud)
- état du cluster Nomad
- état du cluster Consul
- état du cluster Stolon
- état des backups
- crash dans le catalog consul / les allocs nomad
