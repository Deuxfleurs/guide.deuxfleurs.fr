---
title: "aws-cli"
description: "Publier avec aws-cli"
date: 2022-09-01
weight: 20
extra: 
  parent: "prise_en_main/publier-le-contenu.md"
---

Nous allons désormais verser votre site sur Garage, ce dernier le servira à toutes les personnes qui voudront le voir. Vous aurez besoin de l'identifiant de votre clé d'accès et de la clé d'accès secrète, obtenus dans la partie «[Initialiser votre accès](@/prise_en_main/initialiser-votre-accès.md)».

### Paramétrer votre accès localement
Pour verser votre site sur Garage, nous allons utiliser l'outil de base pour faire des commandes S3 : [aws-cli](https://github.com/aws/aws-cli) et son binaire, `aws`.

Vous pouvez installer `aws` :
- soit via les paquets fournis par votre distribution (par ex. [Ubuntu](https://snapcraft.io/aws-cli) ou [Fedora](https://packages.fedoraproject.org/pkgs/awscli2/awscli2/))
- soit en suivant les instructions sur [le site officiel d'Amazon](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).

Une fois `aws` installé sur votre machine, vous pouvez suivre les instructions de connexion indiquées sur [le guichet](https://guichet.deuxfleurs.fr/website/configure#). Cliquez sur le lien "`awscli`" dans la section "Configurez votre logiciel" pour afficher les instructions.

### Configurer la page par défaut et celle pour les erreurs
Puisqu'on est sur la configuration S3, profitons-en pour paramétrer une page d'accueil et une page d'erreur.

En effet, les URL qu'on utilise pour naviguer sur votre site correspondront à la hiérarchie de fichiers présents sur Garage. Si on visite `https://votre-site.fr/blog/recette-de-gateau.html`, Garage va simplement servir le fichier `blog/recette-de-gateau.html`, en partant de la racine du bucket. Mais que ce passe-t-il si on demande à voir `https://votre-site.fr/blog/` ? Garage ne peut pas retourner un dossier; et de toute façon cela ne fonctionnerait pas, puisqu'un dossier n'a pas données propre à lui-même, il ne fait que contenir des fichiers distincts en son sein. On voit pourtant souvent ce genre d'URL en ligne. En fait, les serveurs web sont configurés pour que si aucun fichier du dossier n'est spécifié, alors on va utiliser celui avec un nom paramétré à l'avance. C'est de ça dont on parle.
De même, lorsque quelqu'un demande une page qui n'existe pas, que peut faire Garage ? Dans la même logique, on va lui donner un nom de fichier à servir par défaut si jamais cela arrive.

Après avoir fait votre `source ~/.awsrc`, faites :
```
aws s3 website exemple-un.fr --index-document index.html --error-document erreur.html
```
si votre `aws` est en version 2.x, ou
```
aws s3api put-bucket-website --bucket exemple-un.fr --website-configuration '
{
  "ErrorDocument": {
    "Key": "/errors/4xx.html"
  },
  "IndexDocument": {
    "Suffix": "index.html"
  }
}
'
```
s'il est en version 1.x . Pensez à remplacer `exemple-un.fr` par votre nom de domaine à vous !

### Publier
On y est ! Avec un terminal, positionnez-vous dans le répertoire qui reflète ce que vous voulez mettre en ligne. Celui-ci devrait contenir des fichiers en `.html`, `.css`, ou `.js`, mais pas de `.md`. Si vous ne l'avez pas fait, faites `source ~/.awsrc`, et lancez ensuite :
```
aws s3 sync --delete . s3://exemple-un.fr
```
Cette commande suppose que vous avez `aws` en version 2. N'oubliez pas de mettre votre nom de domaine à la place d'`exemple-un.fr`.

L'option `--delete` supprime les fichiers distants qui ne sont pas présents localement, ce qui est généralement une bonne idée pour éviter de laisser traîner des vieux fichiers sur les serveurs de Deuxfleurs. Mais attention à ne pas supprimer par mégarde des morceaux de votre site web.

Vos fichiers devraient être téléversés. Une fois le processus fini, vous devriez pouvoir ouvrir un navigateur web tel que Firefox par exemple, taper votre nom de domaine dans la barre URL, et naviguer sur votre site web. En cas de pépin, essayez d'actualiser la page en faisant `ctrl`+`shift`+`r` avec votre clavier, ça vous garantira que votre navigateur récupère le vrai contenu en ligne au lieu de piocher dans le cache local sur votre ordinateur.
