---
title: "FileZilla"
description: "Publier avec FileZilla"
date: 2023-09-16
weight: 50
extra: 
  parent: "prise_en_main/publier-le-contenu.md"
---

Pour servir votre site sur Garage via SFTP, vous aurez besoin de votre nom d'utilisateur ainsi que du mot de passe utilisé pour se connecter au [guichet Deuxfleurs](https://guichet.deuxfleurs.fr).

Les dispositions qui suivent peuvent potentiellement fonctionner avec n'importe quel client SFTP. Si vous n'en connaissez pas, nous vous recommandons d'utiliser [FileZilla](https://filezilla-project.org/download.php?type=client).

Après avoir installé FileZilla Client (à ne pas confondre avec FileZilla Server), vous pouvez paramétrer les accès à Garage comme suit :
* ouvrez le Gestionnaire de sites (via le premier bouton à gauche de la barre d'outils, ou le menu Fichier, ou le raccourci `Ctrl+S`)
* choisissez le protocole "*SFTP - SSH File Transfer Protocol*"
* renseignez l'hôte `bagage.deuxfleurs.fr` et le port *2222*
* choisissez le type d'authentification "*Demander le mot de passe*"
* renseignez le même nom d'utilisateur que celui que vous utilisez sur le guichet Deuxfleurs

En cliquant sur "Connexion", une pop-up apparaîtra pour vous demander un mot de passe. Renseignez le même mot de passe que celui que vous utilisez sur le guichet Deuxfleurs. Vous pouvez désormais vous connecter à votre site.

Lorsque vous serez connecté, vous verrez affichés vos différents sites dans la moitié droite de la fenêtre de FileZilla, et les fichiers de votre ordinateur dans la moitié gauche. Vous pourrez ainsi verser ou télécharger les fichiers de votre ordinateur vers vos sites sur Garage et vice-versa.
