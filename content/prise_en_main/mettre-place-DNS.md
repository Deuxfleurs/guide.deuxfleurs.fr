---
title: "Mettre en place le DNS"
description: "Mise en place du nom de domaine"
date: 2022-09-01
weight: 3
extra:
  parent: "prise_en_main/web.md"
---

# Louer son nom de domaine 
Les noms de domaine sont gérés et loués par les _registraires de nom de domaine_. Ce sont eux qui proposent aux clients finaux de réserver des noms. On peut citer [Gandi](https://www.gandi.net/fr), [OVHcloud](https://www.ovhcloud.com/fr/), ou [Scaleway](https://www.scaleway.com/fr/) par exemple. Lorsqu'on veut s'offrir `exemple-un.fr` notamment, c'est chez eux que ça se passe; ensuite, à leur tour, ils vont enregistrer notre demande auprès de l'organisme correspondant à l'extension choisie : l'association française [AFNIC](https://www.afnic.fr/) pour `.fr`, l'association états-unienne [Public Internet Registry ](https://thenew.org/) pour `.org`, l'association européenne (belge en particulier) [EURid](https://eurid.eu/fr/) pour `.eu`... Nous recommandons de privilégier l'une de ces trois extensions parmis la myriade qui existe, car ces entités ont pour historique de ne pas augmenter arbitrairement les prix tout en garantissant la disponibilité de votre nom de domaine.

Pour référence, la location d'un nom de domaine en `.fr` est d'environ 12 euros par an. Certaines offres vous fournissent «des extras» en plus comme une boîte courriel, sans que ça n'influe grandement sur les prix. En ce qui nous concerne, Gandi est un des registraires que nous recommandons.

## Utiliser directement le nom de domaine ou un sous-domaine ?

Une question importante est de choisir si on veut utiliser directement son nom de domaine (`example.com`)
ou alors un sous-domaine, par exemple `blog.example.com`.

Utiliser un sous-domaine comme `www.example.com` ou `blog.example.com` fonctionnera chez tous les hébergeurs DNS
et avec tous les logiciels DNS standards.

À l'inverse, utiliser le nom de domaine directement (c'est-à-dire l'apex, ici `example.com`) impose certaines contraintes techniques. Celles-ci sont détaillées dans la page [CNAME à l'apex](@/prise_en_main/DNS-CNAME-apex.md).
Tous les registraires de nom de domaine ne proposent pas de champs `ALIAS` comme expliqué. C'est cependant le cas de Gandi et de Scaleway.

Pour configurer l'hébergement de votre site derrière le nom de domaine simple, ou apex :
- [C'est par ici si vous êtes chez Gandi](@/prise_en_main/configurer_apex_Gandi.md)
- [C'est par ici si vous êtes chez Scaleway](@/prise_en_main/configurer_apex_Scaleway.md)
