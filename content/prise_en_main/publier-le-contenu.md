---
title: "Publier le contenu"
description: "Comment mettre en ligne son contenu"
date: 2022-09-01
weight: 5
sort_by: "weight"
extra:
  parent: "prise_en_main/web.md"
---

Nous allons désormais verser votre site sur Garage, et ce dernier le servira à toutes les personnes qui voudront le voir. Vous aurez besoin de l'identifiant de votre clé d'accès et de la clé d'accès secrète, obtenus dans la partie «[Initialiser votre accès](@/prise_en_main/initialiser-votre-accès.md)».

Si vous utilisez Hugo, vous pouvez le configurer pour envoyer directement le site sur les serveurs de Deuxfleurs:

* [Publier un site web sur Deuxfleurs avec Hugo](@/prise_en_main/deploiement_hugo.md)

Dans les autres cas, vous avez le choix entre trois méthodes :
* [Faire ça en ligne de commande avec `aws-cli`](@/prise_en_main/aws-cli.md). C'est la méthode la plus habituelle et la plus éprouvée, cependant il faut utiliser le terminal, et être sur linux est grandement conseillé pour celle-ci.
* [Faire ça avec l'utilitaire graphique `rclone`](@/prise_en_main/rclone.md). Cette méthode vous paraîtra peut-être moins intimidante grâce à l'interface graphique du logiciel rclone. Celui-ci tourne sur Linux, Windows, et macOS.
* [Faire ça avec l'utilitaire graphique `winscp`](@/prise_en_main/winscp.md). Cette méthode propose elle également une interface graphique, mais ne fonctionne que sur Windows.

Il existe aussi d'autres méthodes plus proches de celles habituellement utilisées pour verser des pages web en ligne :
* [En SFTP avec FileZilla](@/prise_en_main/filezilla.md). Ce logiciel bien connu fonctionne sur Linux, Windows et macOS.
* [En WebDAV avec votre navigateur de fichiers](@/prise_en_main/webdav.md). Avec cette méthode, vous pourrez manipuler les fichiers de votre site comme s'ils étaient sur votre ordinateur. Pour l'instant, seuls les gestionnaires de fichier Linux sont évoqués ici.

### Bravo !
Une fois l'un de ces trois guides suivi, vous aurez désormais votre propre site web accessible publiquement en ligne !

Vous pouvez rajouter dessus le badge attestant fièrement son hébergement sur Garage:

[![Badge indiquant qu'un site est hébergé sur Garage en français](/img/garage_fr.png)](https://garagehq.deuxfleurs.fr/)
[![Badge indiquant qu'un site est hébergé sur Garage en anglais](/img/garage_en.png)](https://garagehq.deuxfleurs.fr/)

Rajoutez l'image dans votre stockage, et mentionnez-la dans votre contenu.
En HTML, si vous avez écrit votre site à la main :
```
<a href="https://garagehq.deuxfleurs.fr/"><img src="garage_fr.png" alt="Badge indiquant que ce site est propulsé par le logiciel Garage" title="Site propulsé par Garage"></a>
```
En Markdown, si vous utilisez un générateur :
```
[![Badge indiquant qu'un site est hébergé sur Garage](/img/garage_fr.png)](https://garagehq.deuxfleurs.fr/)
```

Vous pouvez aussi revendiquer votre hébergement par Deuxfleurs avec les badges suivants :

[![Badge indiquant qu'un site est hébergé chez Deuxfleurs en français](/img/deuxfleurs_fr.png)](https://deuxfleurs.fr/)
[![Badge indiquant qu'un site est hébergé chez Deuxfleurs en anglais](/img/deuxfleurs_en.png)](https://deuxfleurs.fr/)

Avec les mentions suivantes, en HTML :
```
<a href="https://deuxfleurs.fr/"><img src="deuxfleurs_fr.png" alt="Badge indiquant que ce site est hébergé par l'association Deuxfleurs" title="Site hébergé par Deuxfleurs"></a>
```
Ou en Markdown :
```
[![Badge indiquant qu'un site est hébergé par l'association Deuxfleurs](/img/deuxfleurs_fr.png)](https://deuxfleurs.fr/)
```
