---
title: "Sous Windows (avancé)"
description: " Rclone Mount"
weight: 20
date: "2022-03-28T09:28:19.870Z"
dateCreated: "2022-02-01T16:23:58.635Z"
extra:
  parent: 'prise_en_main/stockage.md'
---

Nous aurons besoin de télécharger 3 programmes :
  - [winfsp](http://www.secfs.net/winfsp/rel/) - téléchargez l'installateur et installez normalement
  - [rclone](https://rclone.org/downloads/) - Téléchargez le zip pour Windows + Intel/AMD 64 bit, puis extraire le fichier `rclone` dans `C:\Program Files\rclone\rclone.exe` (il faut créer le dossier rclone dans Program Files).
  - [nssm](https://nssm.cc/download) - Téléchargez la version "Featured pre-release", actuellement `nssm 2.24-101-g897c7ad (2017-04-26)`. Extrayez le fichier `nssm.exe` dans `C:\Program Files\rclone\nssm.exe`
  
Créez un fichier de configuration `rclone.conf` toujours dans le même dossier (`C:\Program Files\rclone\`) :

```
[garage]
type = s3
provider = Other
env_auth = false
access_key_id = <REMPLACEZ PAR VOTRE IDENTIFIANT DE CLE D'ACCESS>
secret_access_key = <REMPLACEZ PAR VOTRE CLE D'ACCES SECRETE>
region = garage
endpoint = garage.deuxfleurs.fr
bucket_acl = private
force_path_style = true
no_check_bucket = true
```
Ouvrez un terminal Windows (Powershell ou cmd.exe) dans le dossier `C:\Program Files\rclone` toujours. (Clic droit sur le bouton démarrer, puis cherchez Terminal ou Powershell admin, puis tapez `cd "C:\Program Files\rclone"`).

Ajoutez rclone comme service Windows :

```
./nssm.exe install rclone
```

Dans la fenêtre qui s'ouvre :
  1. Dans `Path` (ou Chemin), cliquez sur les 3 points à droite et choisissez `rclone.exe`, vous devriez avoir `C:\Program Files\rclone\rclone.exe`
  2. Laisser `Startup directory` par défaut
  3. Mettez dans `Arguments` (ou Paramètres) ce qui suit : `--config=rclone.conf mount garage: \\deuxfleurs\garage --vfs-cache-mode full`
  4. Tout en bas, dans `Service Name` (ou nom du service), tapez `garage` 
  5. Cliquez sur l'onglet `Log on` (ou Connexion), et cochez "Allow service to interact with desktop" (Autoriser le service à intéragir avec le bureau ou quelque chose du genre).
  6. Cliquez sur `Install service` (ou Installer le service)
  
  
Maintenant on va démarrer le service. Faites clic droit sur le bouton démarrer de Windows puis cliquez sur "Executer", tapez `services.msc` puis faites entrée.

Dans la liste, cherchez "garage", faites clic droit dessus, puis "Démarrer".

Ouvrer votre explorateur de fichier windows (raccourci clavier touche windows + E), normalement un nouveau disque réseau est apparu. Il sera là automatiquement à chaque démarrage.

