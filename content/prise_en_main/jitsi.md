---
title: "Visioconférence"
description: "La visio avec Jitsi"
weight: 40
extra:
  parent: 'prise_en_main/_index.md'
---
Jitsi est un logiciel de **visioconférence**, que Deuxfleurs fournit **en accès libre**.

# Accéder à Jitsi

-   [<span aria-hidden="true">🌐</span> Accéder via le navigateur](https://jitsi.deuxfleurs.fr)
-   [<span aria-hidden="true">📥</span> Application Android (F-Droid)](https://f-droid.org/en/packages/org.jitsi.meet/)
-   [<span aria-hidden="true">📥</span> Application Android (Google Play Store)](https://play.google.com/store/apps/details?id=org.jitsi.meet&hl=fr)
-   [<span aria-hidden="true">📥</span> Application iOS](https://apps.apple.com/fr/app/jitsi-meet/id1165103905)

# Conseils d'utilisation

La visioconférence est un service particulièrement tatillon à proposer bénévolement, surtout depuis des connexions domestiques.
Voilà quelques conseils pour vous assurer une bonne expérience :

### Quand ça rame (vidéo intermittente, son dégradé...)

#### Diminuer la bande passante

<!-- ![Impression d'écran du menu « Paramètres de performance »](/img/jitsi_parametres_de_performance.png) -->
- Vous et vos correspondant⋅es pouvez ajuster vos « Paramètres de performance » pour échanger un flux vidéo plus léger. Vous trouverez ce menu dans les trois petits points « … Plus d'actions ».
- Vous n'avez peut-être pas besoin de la vidéo de tout le monde. Désactivez la vidéo ou gardez-la seulement pour certains participant⋅es.
- Si vous ne parlez pas, vous pouvez également couper votre microphone. Il est possible de passer en mode _talkie-walkie_ sur ordinateur avec la touche espace. Vous maintenez la touche espace, votre micro est activé. Vous arrêtez d'appuyer sur la touche espace, votre micro est coupé.

#### Améliorer la connexion

- Connectez de préférence votre ordinateur en filaire (cable ethernet) à votre *box* (routeur internet).
- Si vous utilisez la communication sans fil (Wi-Fi), rapprochez-vous si possible de votre *box*.
- Le type de connexion internet influencera également la qualité de votre visioconférence : la fibre est idéale, l'ADSL ou les réseaux mobiles (4G, 5G etc.) sont plus incertains.


### Bien se faire entendre

- L'idéal est d'avoir un casque avec microphone ou des écouteurs avec microphone pour capter le son au plus près de votre bouche et _empêcher l'écho_ (que votre microphone capte ce qui sort sur les hauts-parleurs).
- Les micros intégrés aux ordinateurs portables récents captent généralement mieux le son que ceux des anciens.
- Si vous êtes plusieurs sur un seul ordinateur, tenez compte du fait que les microphones sont directionnels : la personne qui parle doit être en face de l'ordinateur.

### Mes correspondant⋅es sont connecté⋅es, mais je ne vois ni n'entends rien

Essayez de :

- Changer de navigateur web : [Firefox](https://www.mozilla.org/fr/firefox/new/), [Chromium](https://chromium.woolyss.com/download/)...
- Changer de connexion : connexion Internet d'un _smartphone_, Wi-Fi...
- Changer de pérhipérique.

Si rien de cela ne résoud le problème, il est temps de trouver une autre adresse pour votre visio d'aujourd'hui. Navré. <span aria-hidden="true">¯\\\_(ツ)\_/¯</span>
