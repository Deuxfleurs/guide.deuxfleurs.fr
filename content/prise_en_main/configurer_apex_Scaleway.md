---
title: "Configurer l'apex chez Scaleway"
description: "Paramétrer l'entrée DNS directement sur le nom de domaine de base"
date: 2023-12-04
weight: 2
extra:
  parent: "prise_en_main/mettre-place-DNS.md"
---

Supposons que vous avez loué `camille-michu.fr` chez Scaleway.

D'abord, rendez-vous dans votre espace utilisateur Scaleway. Sur la liste à gauche de l'écran, cliquez sur «Domains and DNS» dans la section «Network». Vous devriez arriver sur l'écran suivant. Cliquez sur votre nom de domaine :

![dns1.png](/img/apex_scaleway_1.png)

Une fois ici, cliquez sur l'onglet «Zones DNS» :
  
![dns2.png](/img/apex_scaleway_2.png)

Dans l'écran suivant, cliquez sur «Zone racine» :

![dns3.png](/img/apex_scaleway_3.png)

Ensuite, cliquez sur le bouton «Ajouter des records» :

![dns4.png](/img/apex_scaleway_4.png)

Une fenêtre va apparaître au centre de l'écran. Remplissez là exactement comme suit. Attention, dans le champ «Nom de l'hôte», `garage.deuxfleurs.fr` doit être suivi d'un point, sans espace, comme illustré, ne l'oubliez pas ! Vous pouvez après cliquer sur le bouton «Modifier le record».

![dns5.png](/img/apex_scaleway_5.png)

Vous devez alors vous retrouver face à un écran similaire au suivant :

![dns5.png](/img/apex_scaleway_6.png)

Félicitation, vous avez réussi ! Maintenant que votre configuration DNS est réglée, vous pouvez [préparer votre contenu](@/prise_en_main/creer-du-contenu.md) !

