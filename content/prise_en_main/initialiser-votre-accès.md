---
title: "Initialiser votre accès"
description: "Initialiser le stockage objet sur Garage"
date: 2022-09-01
weight: 2
extra:
  parent: "prise_en_main/web.md"
---

De par l'utilisation de son logiciel Garage, Deuxfleurs conserve et sert les sites web en recourant au stockage objet. Pour chaque site, on a un bucket (seau en français), partageant le même nom que lui. On peut voir un bucket comme un récipient dans lequel on va mettre tout nos fichiers à publier. Créer un bucket ne prend que quelques clics depuis votre compte DeuxFleurs.

# Créer un bucket pour héberger son site web

* En bas de l'[interface de votre compte](https://guichet.deuxfleurs.fr), cliquez sur "Mes sites Web".
* Cliquez sur "Nouveau site web".
* Spécifiez le nom de domaine pour votre site (l'identifiant du site qu'on tape dans la barre URL d'un navigateur pour y accéder) :
	* Si vous n'avez pas votre propre nom de domaine, votre site peut être rendu accessible dans un sous-domaine de `web.deuxfleurs.fr`. Dans l'onglet "Je n'ai pas de nom de domaine", entrez le sous domaine de votre choix.
	* Si vous avez votre propre nom de domaine, entrez-le dans l'onglet "Utiliser mon propre nom de domaine". Il faudra alors [configurer votre serveur DNS pour pointer sur l'adresse d'hébergement chez DeuxFleurs](@/prise_en_main/mettre-place-DNS.md).
* Cliquez sur "Créer un nouveau site web" pour confirmer la création du bucket dans Garage.

Pour copier les fichiers du site web dans le bucket et [publier le site web](@/prise_en_main/publier-le-contenu.md), vous aurez besoin des clefs d'accès et des propriétés du bucket accessibles en cliquant sur "Mes identifiants".

Les clefs d'accès permettent d'assurer que vous seul·e·s (ou celleux à qui vous partagez les clefs) peuvent modifier le contenu du site.
 - L'identifiant de votre clé d'accès ressemble par exemple à `cSGcEQoInTVgkxPIzzF6IyU7C7`. Même s'il vaut mieux ne pas le crier sur tous les toits, il n'y a pas de risque de sécurité si vous le dévoilez à des gens.
 - La clé d'accès secrète qui ressemble à `rCE6vzSVkll54FNd0Jvx7uoRsNqAXexxX1b3rm2BofquQKln6bdnDLRRli1dZbSS`, elle, comme son nom l'indique, doit absolument rester secrète. Si elle est dévoilée par accident, contactez un·e administrateur·rice immédiatement !

# Du point de vue des administrateur·ice·s

Pour les administrateur·ice·s, le bucket correspondant au site web apparaît comme :

```
$ sudo docker exec -ti 78e6b7e53241 /garage bucket list|grep -P 'exemple-un.fr|exemple-deux.eu'
                                           4bdce6:exemple-un.fr                  56a0f10929f0d63d0177415a22a0b636cf1efe92845f2b00e04b91298656e78e
  exemple-deux.eu                                                                941956ebfffa8fd564ad6b40e4bb5ac2f2b93bdbba1d1bf88fe6daff2cf4df6c
```
Si `exemple-un.fr` n'est pas à gauche dans la même colonne que `exemple-deux.eu`, c'est parce qu'il n'est pas réglé en global. L'administrateur·rice va corriger ça en faisant `garage bucket alias 56a0f10929f0d63d0177415a22a0b636cf1efe92845f2b00e04b91298656e78e exemple-un.fr`. Résultat :
```
  exemple-un.fr                                4bdce6:exemple-un.fr              56a0f10929f0d63d0177415a22a0b636cf1efe92845f2b00e04b91298656e78e
  exemple-deux.eu                                                                941956ebfffa8fd564ad6b40e4bb5ac2f2b93bdbba1d1bf88fe6daff2cf4df6c
```
