---
title: "WebDAV"
description: "Publier via un gestionnaire de fichiers avec WebDAV"
date: 2023-09-16
weight: 60
extra: 
  parent: "prise_en_main/publier-le-contenu.md"
---

Comme avec [le SFTP](@/prise_en_main/filezilla.md), vous aurez besoin pour utiliser le WebDAV de votre nom d'utilisateur ainsi que du mot de passe utilisé pour se connecter au [guichet Deuxfleurs](https://guichet.deuxfleurs.fr).

**Attention cependant : si vous venez de créer un site via le guichet Deuxfleurs, celui-ci ne pourra être modifié via WebDAV tant que vous n'avez pas ajouté un fichier dessus via [les autres méthodes décrites dans ce guide](@/prise_en_main/publier-le-contenu.md).**

### Paramétrer l'accès WebDAV avec le gestionnaire de fichiers GNOME (Nautilus)

Si vous utilisez Ubuntu ou Fedora, votre gestionnaire de fichiers par défaut est probablement celui de GNOME, également appelé Nautilus. Voici comment accéder à vos sites en WebDAV :
* dans la barre latérale de Nautilus, cliquez sur "Autres emplacements" (tout en bas)
* dans le champ "Connexion à un serveur", saisissez `davs://bagage.deuxfleurs.fr/webdav` et cliquez sur "Se connecter"
* renseignez les mêmes nom d'utilisateur  et mot de passe que deux que vous utilisez sur le guichet Deuxfleurs

Un nouveau dossier "bagage.deuxfleurs.fr" apparaîtra dans la barre latérale de Nautilus : c'est lui qui affiche le contenu de votre espace sur Garage. Vous pouvez interagir avec de la même façon qu'avec les fichiers sur votre ordinateur.

### Paramétrer l'accès WebDAV avec le gestionnaire de fichiers KDE (Dolphin)

TODO

### Paramétrer l'accès WebDAV avec le gestionnaire de fichiers Linux Mint (Nemo)

TODO
