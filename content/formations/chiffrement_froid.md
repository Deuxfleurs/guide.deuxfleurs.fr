---
title: "Chiffrement à froid"
description: "Guide à l'installation d'une machine en chiffrant intégralement son disque dur"
weight: 30
date: 2022-01-30T20:43:56.953Z
dateCreated: 2021-12-29T15:29:02.943Z
extra:
  parent: 'formations/sysadmin.md'
---

Le chiffrement à froid, c'est le fait de n'inscrire que des données chiffrées sur un disque (plus précisément [une partition de disque](https://fr.wikipedia.org/wiki/Partitionnement_logiciel_/_mat%C3%A9riel)). C'est le standard [LUKS](https://fr.wikipedia.org/wiki/LUKS) qui permet ce chiffrement : ce dernier se place entre le support de stockage et le système d'exploitation (OS), et (dé)chiffre tout ce qui est lu ou écrit sur une partition. **Cette étape doit donc être réalisée avant d'installer le système d'exploitation, mais après [le partitionnement de vos disques](@/formations/partitionnement.md).**

Une fois le chiffrement mis en place, on utilise le système d'exploitation comme d'habitude (tout a l'air déchiffré à l'usage), parce que LUKS chiffre et déchiffre - de façon transparente - toutes les informations que l'OS lit et écrit sur la mémoire morte (le disque).

L'intérêt, c'est que, si quelqu'un part avec votre disque dur - il ne pourra pas lire son contenu à moins de vous avoir extorqué la clé. 

Dans le cas d'un **hébergement en centre de données**, votre hébergeur a accès à vos disques - c'est donc une bonne première ligne de défense que de **chiffrer son disque, afin que l'hébergeur ne puisse pas y lire comme dans un livre ouvert.** 
Néanmoins, ne croyez pas que le chiffrement à froid soit une mesure *suffisante* pour empêcher votre hébergeur d'accéder à vos données : déterminé, il y arrivera - ça lui prendra seulement plus de travail.
**La solution, pour garantir qu'on est seul⋅e à avoir accès à ses données, c'est l'auto-hébergement.**


Les aspects négatifs, c'est que :

* Toutes ces opérations cryptographiques (dé/chiffrement) ont un coût, qui peut être non-négligeable sur un vieil ordinateur disposant d'un processeur hors d'âge.
* Avant chaque démarrage du système, il faut fournir la clé (le mot de passe) qui débloquera les partitions disque (sur lesquelles se trouvent l'OS qu'on espère démarrer et nos données). 

    **On se demande bien ce qu'on va faire du mot de passe - s'agirait pas de le perdre !**

    Des outils comme [clevis](https://github.com/latchset/clevis) existent pour automatiser le déverouillage d'ordinateurs disants, mais c'est hors du cadre de ce tutoriel.
    
    Pour le moment, on se contentera d'installer de quoi déverouiller manuellement le disque à distance (SSH dans initrd).
    

> TODO

## Chiffrement de la mémoire

Certains processeurs AMD récent disposent d'une fonctionalité de chiffrement à la volée de la mémoire. Cela permet d'éviter - en théorie du moins - les attaques physique per lecture du cotenue de la mémoire. Plus d'informations sont disponible [en anglais dans cet article](https://mricher.fr/post/amd-memory-encryption/).

# Références

* cryptsetup, [Frequently Asked Questions](https://gitlab.com/cryptsetup/cryptsetup/-/wikis/FrequentlyAskedQuestions), Dépôt Gitlab de `cryptsetup`, anglais.
* ADRN, [Guide d'installation de Debian avec chiffrement à froid dans le centre de données Kimsufi](https://plume.deuxfleurs.fr/~/WebTrotter/installing-a-cloud-server-with-full-disk-encryption), Blog Deuxfleurs, avril 2021, anglais.

