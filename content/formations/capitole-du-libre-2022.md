---
title: Capitole du Libre 2022
weight: 80
draft: false
date: 2023-04-01
extra:
  parent: formations/conf.md
---
**De l'auto-hébergement à l'entre-hébergement : Garage, pour conserver ses données ensemble**
                                    
_Garder le contrôle de ses données est essentiel pour reconquérir sa vie 
privée et ses libertés sur internet. L'auto-hébergement de ses services 
est un moyen d'y parvenir, mais administrer et sauvegarder ses données 
tout seul représente un défi. Cette présentation met en avant le 
logiciel Garage, qui simplifie ces tâches et permet de collaborer entre 
hôtes, ainsi que son utilisation au sein de notre structure Deuxfleurs._            

Assurer la sauvegarde, l'intégrité, et la disponibilité de données 
représentent un défi pour quiconque veut auto-héberger ses services. Les
 CHATONS, c'est-à-dire les hébergeurs indépendants, ne sont pas épargnés
 ; les grands acteurs d'internet non plus. De plus en plus, le stockage 
objet est privilégié en lieu et place des hiérarchies de systèmes de 
fichiers, car il popularise des avantages pratiques pour les 
administrateurs : réplication, sommes de contrôle, etc. Ce paradigme est
 souvent interfacé avec le standard ouvert S3, où le logiciel 
utilisateur devient agnostique de toute préoccupation concernant la 
gestion des données.
Actuellement en fonctionnement au sein de l'infrastructure de 
Deuxfleurs, nous avons développé Garage, un gestionnaire de données, 
capable de les répliquer efficacement sur plusieurs sites pour se parer 
contre les imprévus. Publié sous licence AGPL, il est conçu pour avoir 
de faibles prérequis en termes de matériel et d'environnement réseau, le
 rendant pratique à utiliser dans un contexte non professionnel, par 
exemple sur des machines d'occasion derrière des connexions internet de 
particuliers. Les services (par exemple Peertube, Nextcloud, ou Matrix, 
pour citer parmi ceux testés) se contentent simplement d'utiliser l'API 
S3 pour quêter Garage, qui prend en charge toute la gestion nécessaire à
 l'exploitation de données.
Au-delà de l'aspect technique des choses, ce changement structurel sur 
le plan opérationnel recèle selon nous bien des perspectives quant aux 
potentiels d'internet. Nous avons la conviction qu'en repensant de la 
sorte l'organisation des acteurs sur cet espace, on peut façonner un 
bien meilleur rapport entre l'humain et ses outils numériques, voire 
entre les individus eux-mêmes.






Rediffusion vidéo : [https://invidious.fdn.fr/watch?v=jI_vPVji51c](https://invidious.fdn.fr/watch?v=jI_vPVji51c)

Support de présentation : [https://git.deuxfleurs.fr/Deuxfleurs/garage/raw/branch/main/doc/talks/2022-11-19-Capitole-du-Libre/pr%C3%A9sentation.pdf](https://git.deuxfleurs.fr/Deuxfleurs/garage/raw/branch/main/doc/talks/2022-11-19-Capitole-du-Libre/pr%C3%A9sentation.pdf)

Les autres talks sur Garage : [https://git.deuxfleurs.fr/Deuxfleurs/garage/src/branch/main/doc/talks](https://git.deuxfleurs.fr/Deuxfleurs/garage/src/branch/main/doc/talks)

Abstract dans la conférence : [https://cfp.capitoledulibre.org/cdl-2022/talk/GNAKPS/](https://cfp.capitoledulibre.org/cdl-2022/talk/GNAKPS/)
