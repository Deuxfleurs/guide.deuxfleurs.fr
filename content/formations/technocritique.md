---
title: Devenez technocritique
weight: 10
draft: false
date: 2023-06-14
extra:
  parent: formations/animations.md
---
**Objectif :** Se sentir légitime à prendre la parole sur des sujets techniques ; ouvrir à des arguments de l'ordre du besoin, des usages, de l'expérience ; introduire à des auteur·ices de la critique des techniques (de Ivan Illich à Fanny Lopez) ; aborder le sujet d'un point de vue structurel, collectif, politique et non individuel.

**Moyen** : Les participant·es recoivent des cartes, de types différents, qui servent à une mise en situation et qui les guident dans l'analyse d'un des services du numérique qu'ils et elles connaissent bien.

**Déroulé** : Compter au moins 1 heure. 5 min pour l'arrivée ; 5 min d'introduction ; 30 min de réflexion ; 20 min de restitution. 1h30 serait probablement plus confortable.

# Déroulé en détail

**En amont** : Imprimer les cartes ou penser à les prendre. Préparer des groupes de cartes (1 carte Situation, 1 carte Ça Dérape, 1 carte Greenwashing) allant bien ensemble car toutes n'ont pas les mêmes synergies. Prendre des feuilles et des stylos semble être une bonne idée également pour soi et pour les participant·es.

**Introduction** : Se lancer rapidement dans le “jeu” donc aller droit au but. Commencer par prévenir qu'il va falloir faire des groupes (environ 4 c'est bien). Présenter les objectifs, le moyen, introduire les cartes.

**Lancer le jeu** : Aider les groupes à finir de se constituer, tourner dans la salle pour les aider à démêler leurs pensées.

**Restitution :** Chaque groupe / chaque personne fait un retour. Le groupe présente son cas d'analyse et ses recommendations pour améliorer la situation. Définir en amont le temps alloué (tout en gardant 2 minutes pour l'ouverture). Par exemple 4 minutes / groupe.

**Ouverture :** Noter que “Technocritique” est le nom d'une collection au Seuil (éditeur). Avoir quelques références à citer : Ivan Illich La Convivialité, André Gorz, (Ellul), François Jarrige, Fanny Lopez, “Technologie partout, démocratie nulle part” du Mouton Numérique, etc.

# Les cartes

## Situation

_La situation est définie comme l'intersection d'un besoin et un contexte en même temps. Lors de l'échange, ces cartes permettent à la fois d'interroger le besoin (est-ce qu'on a vraiment besoin d'investir autant d'efforts dans la satisfaction de ce besoin ?), mais aussi le contexte (est-ce qu'il n'est pas compliqué de satisfaire ce besoin à cause de l'environnement ?)._

**Échanger**

_Rester en contact avec ses proches._

Vous partez loin pour rejoindre votre conjoint·e. Vous voulez rester en contact avec votre famille et partager des tranches de vie.

**Se réunir**

_Débattre, décider_

Vous faites parti d’un collectif dont les membres habitent loins.
Vous avez besoin d'avancer entre vos réunions physiques.

**Rencontrer**

_Au delà de ses cercles_

Vous avez une passion de niche que votre entourage ne partage pas. Vous souhaitez rencontrer d’autres personnes qui partagent votre intérêt.

**Informer**

_Faire exister_

Vous organisez une grande fête dans votre village. Il vous faut faire connaitre votre évènement, votre programme, et des modalités diverses (le lieu, les heures, le prix, etc.).

**Apprendre**

_Partager et accéder à la connaissance_

Vous commencez un compost Jean Pain et vous voulez profiter de l’expérience d’un maximum de personnes. Vous
comptez bien partager vos apprentissages également.

**Se distraire / s'amuser / jouer**

_Vivre des émotions_

Vous aimez la musique et vous voulez vous tenir au courant de ce qui se fait.
D’ailleurs, vous aimeriez aussi pouvoir mettre en avant la musique que vous aimez.

**S'organiser**

_Enregister, compiler, partager_
Vous devez gérer des bénévoles, de la comptabilité, et tout un tas de choses liées à la logistique de
votre futur évènement. Vous êtes plusieurs sur ce projet.

## Ça dérape

_L'introduction d'un outil technique peut avoir des effets à l'échelle collective comme individuelle. Cette catégorie de carte cherche à brosser quelques un de ces aspects._

**Uniformisation**

_Manque de créativité_

En utilisant cet outil, les gens ont perdu leur créativité d’antan, car l’outil impose un cadre, il restreint certaines actions, et en favorise d’autres.

**Dépendance**

_Perte d’autonomie_

En utilisant cet outil, les gens perdent un certain savoir-faire car l’outil réalise la tâche sans les faire participer.

**Technocratie**

_Des expert·es pour votre bien_

Une classe d’expert·es se sent seule légitime à prendre les décisions sur cet outil, elle crée beaucoup de mécontentement et de défiance.

**Monopole**

_C’est ça ou rien_

Cet outil a été un franc succès, il a été adopté par presque tout le monde au point que la société s’est façonnée au-
tour, et qu’il est devenu impossible, ou très compliqué, de vivre sans.

**Fétichisme**

_Mon précieux_

Cet outil renvoie une belle image et des valeurs positives. Certaines personnes l’utilisent juste parce que c’est cool, que ça fait bien, pour être dans l’air du temps.

**Invisibilisation**

_C’est si loin de nous_

Les gens ne se rendent pas compte du coût environnemental et humain de cet outil et l’utilisent donc sans modération.

## Greenwashing

_L'argument écologique n'est pas toujours sincère, mais il est rare qu'il soit frontalement mensonger. Ces cartes servent à mettre des mots sur des procédés plus subtiles._

**Compromis caché**

_L'avantage annoncé implique un désavantage encore plus grand_

Les publicités d’appareils électroniques dits "écologiques" car économes en énergie occultent le plus souvent l’impact environnemental de la fabrication (énergie grise et pollutions chimiques) et de la fin de vie (le produit
est peut-être plus compliqué à recycler et contient des matières dangereuses).

**Méthodologie manquante**

_Des résultats sont annoncés sans le détails de l'analyse_

Les équipements électroniques ou informatiques qui avancent "une économie d’énergie de 50%" sans preuve de leur prétention ou agrément. Le calcul a pu être fait sur un appareil particulièrement énergivore ou dans une situation particulière.

**Imprécision**

_Des expressions abstraites sont utilisées_

L'expression "sans substances nocives" ne veut rien dire, selon la quantité, toute substance peut devenir nocive. les expressions "vert, "sans danger pour l’environnement" ou "préserve l’environnement" ne veulent rien dire sans explications détaillées.

**Moindre des deux maux**

_Ne tient pas compte de toutes les possibilités_

Microsoft qui, pour la promotion de Windows 7, met en avant ses meilleures capacités de gestion d’énergie, alors que globalement, ce système d’exploitation nécessite un ordinateur 243% plus performant que pour faire tourner
Windows XP ou certaines distributions Linux qui suffisent pour un usage bureautique.

## Ressources

(lien de téléchargement pour le futur).
