---
title: "Diplonat"
description: ""
date: 2021-11-09T12:42:17.716Z
dateCreated: 2021-11-09T12:42:15.729Z
weight: 30
extra:
  parent: 'infrastructures/logiciels.md'
---

# Diplonat

Diplonat est un utilitaire qui facilite l'hébergement de services dans un environnement réseau dynamique. Le cas le plus commun est lorsqu'il faut exposer un serveur situé dans un sous-réseau domestique, derrière un routeur souvent fourni par un FAI grand public. Diplonat aide alors à gérer les règles NAT de la passerelle, les règles de pare-feu du serveur lui-même, et potentiellement l'enregistrement DNS pour qu'il suive l'IP dynamique du sous-réseau.

## Status du développement

Diplonat est en développement, le code est versionné ici : <https://git.deuxfleurs.fr/Deuxfleurs/diplonat>. Il est exploité dans le cadre des activités de deuxfleurs.
