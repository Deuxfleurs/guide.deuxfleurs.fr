---
title: "Services"
description: "Annuaire des services hébergés chez Deuxfleurs"
weight: 10
extra:
  parent: 'infrastructures/_index.md'
---

Cette page tente de recenser de façon exhaustive l'ensemble des services qui
fonctionnent actuellement sur les machines de Deuxfleurs, dans les différents
rôles identifiés : production, développement, expérimentation, etc.

| Service | Rôle | Site | Description |
| -- | -- | -- | -- |
| [Garage](https://garage.deuxfleurs.fr) | production | Orion + Neptune, Bespin, Jupiter | Serveur de stockage de données |
| [deuxfleurs.fr](https://deuxfleurs.fr) | production | *cf.* Garage | Site principal de Deuxfleurs |
| [guide.deuxfleurs.fr](https://guide.deuxfleurs.fr) | production | *cf.* Garage | Ce site |
| Autres sites web | production | *cf.* Garage | Autres sites statiques servis par Garage |
| [Synapse](https://im.deuxfleurs.fr) | production | Orion | Serveur Matrix |
| [Element](https://riot.deuxfleurs.fr) | production | Orion | Client web pour Matrix |
| Postfix | production | Orion | Serveur SMTP |
| Dovecot | production | Orion | Serveur IMAP |
| [SoGo](https://sogo.deuxfleurs.fr) | production | Orion | Client mail SoGo |
| [Alps](https://alps.deuxfleurs.fr) | production | Orion | Client mail Alps (plus léger) |
| [Plume](https://plume.deuxfleurs.fr) | production | Orion | Blog collaboratif et fédéré |
| [Jitsi](https://jitsi.deuxfleurs.fr) | production | Neptune | Service de visioconférence |
| [CryptPad](https://pad.deuxfleurs.fr) | production | Neptune | Éditeur de documents collaboratif chiffré |
| [Guichet](https://guichet.deuxfleurs.fr) | production | Neptune | Interface de gestion des utilisateurs |
| Prometheus | production | Neptune, Bespin | Interface de monitoring de l'infrastructure |
| [Grafana](https://grafana.deuxfleurs.fr) | production | Neptune | Interface de monitoring de l'infrastructure |
| [Forgejo](https://git.deuxfleurs.fr) | développement | Bespin | Forge logicielle |
| [Drone](https://drone.deuxfleurs.fr) | développement | Neptune | Serveur d'intégration continue |
| Drone (runner) | développement | Bespin | Worker pour l'intégration continue |
| SSH | sauvegarde | Mercure | Target de backups (Consul) |
| [Minio](https://s3.deuxfleurs.shirokumo.net) | sauvegarde | Mercure | Target de backups restic |
| [cron rclone](@/operations/rclone.md) | sauvegarde | Jupiter | Backup régulier du contenu de Garage |
| [staging.deuxfleurs.org](https://staging.deuxfleurs.org) | expérimentation | Neptune, Jupiter | Site statique de test |
| Garage ([S3](https://garage.staging.deuxfleurs.org), [K2V](https://k2v.staging.deuxfleurs.org)) | expérimentation | Neptune, Jupiter | Beta-test serveur de stockage de données |
| [Guichet](https://guichet.staging.deuxfleurs.org) | expérimentation | Neptune/Jupiter | Beta-test interface de gestion des utilisateurs |
| Prometheus | expérimentation | Neptune, Jupiter | Interface de monitoring |
| [Grafana](https://grafana.staging.deuxfleurs.org) | expérimentation | Neptune/Jupiter | Interface de monitoring |
| [Jaeger](https://jaeger.staging.deuxfleurs.org) | expérimentation | Neptune/Jupiter | Interface de monitoring |

Une liste de sites séparés par des virgules (e.g. Neptune, Orion) indique un service qui stocke des données
et dont le fonctionnement est simultanément assuré par plusieurs sites pour garantir la disponibilité des données
lorsqu'un des sites est indisponible.

Une liste de sites séparés par des slash (e.g. Neptune/Jupiter) indique un service qui ne stocke pas lui-même
de données, et dont le basculement d'un site à un autre est automatisé en cas de panne.

Sur le cluster de production, notre serveur Garage stocke des données sur les 4 sites (Neptune, Orion, Jupiter, Bespin),
mais l'accès extérieur se fait uniquement par les noeuds de Orion.
