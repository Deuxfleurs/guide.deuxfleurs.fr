---
title: Ré-employer (URSSAF)
weight: 50
draft: false
date: 2023-03-13
extra:
  parent: vie_associative/kb.md
---
La DGFIP nous a fait peur : on a cru qu'on allait devoir remplir une DSN à néant tous les mois du monde, à moins de ne plus jamais employer ! La conclusion : il vallait mieux appeler l'URSSAF : 

> 3957, service gratuit + prix d'appel Depuis l'étranger au +33 9 69 36 00 57

Ils nous ont clôturé notre statut d'employeur. Si l'on souhaitait ré-employer des salarié⋅es, c'est ici que ça se passerait : [https://www.due.urssaf.fr/declarant/formulaireDueLibre.jsf](https://www.due.urssaf.fr/declarant/formulaireDueLibre.jsf)
