---
title: "Groupes d'intérêts"
description: "Groupes d'intérêts"
weight: 10
extra:
  parent: 'vie_associative/_index.md'
---

# Conseil d'administration

Le conseil d'administration s'occupe du cadre légal de l'association.
Il est composé de 3 à 6 membres non spécialisés qui se répartissent les tâches.
Il est élu chaque année pendant l'assemblée générale selon le mode de scrutin dit du jugement majoritaire.

Élu·e : Maximilien, Adrien, et Vincent  
Email : ca (arobase) deuxfleurs.fr  

# Technique

Le groupe technique s'occupe de tout le cycle de vie du logiciel et des machines, de la conception au décomissionnement en passant pas le maintien en condition.
Une partie de ses membres a un statut spécial dit d'opérateur·ices car ils ont accès à des machines gérant possiblement des données personnelles.
Ces membres se sont engagés à respecter une charte et ont été cooptés par des membres existants.
Ce groupe est ouvert à tout le monde, l'adhésion y est de fait, il suffit de rejoindre un des canaux Matrix.

Référent·e : à définir  
Membres ayant le statut d'opérateur·ices : Quentin, Alex, Maximilien, Florian, Adrien, Jill  
Fichiers de références du statut opérateur·ices : [ansible (legacy)](https://git.deuxfleurs.fr/Deuxfleurs/infrastructure/src/branch/main/os/config/roles/users/vars/main.yml) - [nix (beta)](https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/cluster/prod/cluster.nix)

Discuter : Sur Matrix à [#tech:deuxfleurs.fr](https://matrix.to/#/#tech:deuxfleurs.fr), #garage:deuxfleurs.fr, #bottin:deuxfleurs.fr, et #diplonat:deuxfleurs.fr.

# Communication

Le groupe communication s'occupe de faire porter l'action de Deuxfleurs, tant sur le plan graphique, texte, que des idées.
Ce groupe est ouvert à tout le monde, l'adhésion y est de fait, il suffit de rejoindre un des canaux Matrix.

Référent·e : à définir

Discuter : Sur Matrix à [#graphisme:deuxfleurs.fr](https://matrix.to/#/#graphisme:deuxfleurs.fr) et [#forum:deuxfleurs.fr](https://matrix.to/#/#forum:deuxfleurs.fr)
