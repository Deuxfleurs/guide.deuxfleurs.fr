---
title: "Base de connaissance"
description: "Base de connaissance"
weight: 100
extra:
  parent: 'vie_associative/_index.md'
---

Une base de connaissance contient des recettes en vrac, des procédés, des retours d'expériences, etc.
Son objectif est d'éviter d'avoir à passer deux fois par une phase d'apprentissage.
