---
title: "AG 2023"
description: "Quatrième Assemblée Générale"
weight: 40
extra:
  parent: 'vie_associative/ag.md'
---



Le dimanche 12 février 2023, se tenait sur Jitsi la quatrième Assemblée Générale (AG) de Deuxfleurs.

[Lien d'accès aux slides de la présentation](https://pad.deuxfleurs.fr/slide/#/3/slide/view/84b9b62d9c0fa512a36ee49b25f95453/present/)


# En deux mots:

- Une dizaine de membres étaient présents à l'AG
- L’adresse de l'association est changée à Lille chez Adrien.
- Le conseil d'administration 2023 est composé d'Adrien, Maximilien et Vincent, tous ayant fait partie du CA depuis la fondation de l'association.

# Appel à candidatures

Élection au jugement majoritaire du bureau 2023. Trois candidats déclarés : Vincent Giraud, Maximilien Richer, Adrien Luxey-Bitri. Alain ne se représente pas. Pas de nouveaux candidats lors de l'AG. 20 membres bénéficient du droit de vote.

# Présentation de l'association.

L’objectif de l'association Deuxfleurs est de « fabriquer un internet convivial ».

Dans les statuts : association loi 1901 dont l'objectif est de défendre et promouvoir les libertés individuelles et collectives à travers la mise en place d’infrastructures numériques libres.

Fondée en janvier 2023 à Rennes mais existait déjà sans forme juridique avant. 9 membres en 2020, 20 maintenant.

Missions :

- fabriquer des outils pour les communautés (CHATONS)
- proposer des services numériques au grand public
- plaidoyer, inspirer, essaimer

Clarification faite en 2022 de la notion d'usager.ère vs membre.

- Usager.ère = personne qui utilise les services Deuxfleurs, contre un prix libre.
- Membre = personne physique adhérente à l'asso, 10€/an, donne le droit de vote en AG.

Clarification sur les usagers : accès donnés à prix libre à des personnes physiques et morales. Prix indicatif : 15€/an par personne physique. Dans le cas d'une personne morale, multiplier par le nombre de personnes physiques qui utiliseront effectivement les services.

Le prix est LIBRE ! 0€ est possible aussi.

*À faire :* publier les prix sur le site.

Outils proposés en accès libre: CryptPad, Jitsi.

Outils accessibles aux personnes ayant un compte :

- Guichet (annuaire)
- hébergement de site statique (garage)
- Matrix (discussion)
- Plume (blog)
- emails et calendrier avec SoGo

# Bilan moral 2022

**Projet NGI/Garage.**

139000 € de subventions de l'Union Européenne, 3 salariés en CDD durant un an. Chronologie:

- avril 2021 : dépôt de la candidature
- juin 2021 : projet accepté
- août 2021 : contrat de subvention signé
- oct 2021 : début du projet
- février 2022 : 3 jalons validés sur 5
- ---- AG 2022 ----
- mai 2022 : jalon 3a validé
- juillet 2022 : jalon 3b validé
- septembre 2022 : jalon 4 validé
- 1e octobre 2022 : clôture des contrats

Bilan de l'aspect "financier" : on est passés dangereusement près des 0€ sur le compte.

Bilan technique:

- développement d'un logiciel de stockage distribué (Garage)
- support S3 robuste
- utilisé en production (deuxfleurs.fr)
- deux fois sur HackerNews en première page!🔥
- début du support d'une API clef-valeur (`K2V`)
- début de serveur mail (`aerogramme`)
- Garage est aujourd'hui une pierre angulaire de l'infra Deuxfleurs
- communauté florissante qui contribue (v8.2 soon™)

**Deuxfleurs et les CHATONS**

- Deuxfleurs est officiellement un chaton depuis avril 2022.
- participation au Camps CHATONS à l'été 2022
- des premiers usagers commencent à entrer en contact avec nous via chatons.org

**Les camps Deuxfleurs de 2022**

- camp d'été chez Alex en IdF
- camp d'hiver au Couvent des Clarisses à Lille

Ont été l'occasion de discuter, d'échanger, de s'organiser, de partager. Un peu de discussions techniques, mais pas majoritairement.

*À faire :* prévoir le camp d'été 2023 (quand et où?)

**Interventions publiques**

Soit des présentations/conférences données par des personnes de l'asso, soit juste des présences à des événements pour discuter.

- Tarare micro club informatique
- Camp CHATONS au Moulin bleu
- Capitole du Libre 2022
- Podcast : Open Tech Will Save Us
- FOSDEM 2022
- Équipe de recherche STACK à Nantes
- Équipe de recherche SPIRALS à Lille
- Freedom Not Fear
- Open Source Experience
- Équipe de recherche TOCATTA à Saclay

Plein de choses sont possibles, l'association peut soutenir financièrement ou donner du contenu pour les personnes qui voudraient faire ce genre de choses.

**Bilan des objectifs techniques 2022**

Pas mal d'achats de matériel qui ont permi de stabiliser des infras et d'essaimer des petits ordinateurs partout.

Migration GlusterFS → Garage, Traefik → Tricot. Fiabilisation des services. Idée qu'on peut produire nous-même des outils, moins complets mais plus adaptables à notre config.

Mise en place de sites statiques, infra multisite, métriques.

Meilleure sécurisation des accès opérateur.ice. On a maintenant une charte operateur.ice à signer.

*À faire :* énormément de choses. Par exemple : base de données multisite, soucis de stockage local (CryptPad, Plume, email), alerting. Projets en cours notamment sur les courriels (Aérogramme) et sur plume.

# Bilan comptable 2022

Solde au 01/01/2022 : 53 122,70€

Revenus: 120 981 €

- NGI/Garage : 120 716 €
- Cotisations : 170€
- Dons : 95 €

Dépenses : 158 872 €

- NGI/Garage : 158 673 €
- Frais : 198 €

Balance de l'année 2022 : -37 891,79 €

Solde au 01/01/2023 : 15 230,91€

Dépenses de NGI en 2022 (découpage des 158 673 €) :

- Salariat (incl. comptable, cotisations, mutuelle, etc) : 148 196 €
- Prestations : 5 000 €
- Évenements (3 camps) : 3 066 €
- Achat matériel : 1 674 €
- Frais pro : 601 €
- Achat stickers : 36 €

# Perspectives 2023 et motions

**Mise en place de cercles**

Structuration des activités de l'asso autours d'un nombre de cercles, par exemple : CA, usages, infra, essaimage, dev, gestion, organisation des camps AFK, etc.

On avait lancé ça en 2022, c'est encore balbutiant.

Principe d'un cercle :

- il y a un référent qui s'occupe d'avoir les infos (pas de pouvoir de décision)
- réunions récurrentes planifiées
- com interne (souvent un salon Matrix)
- se rendre visible (e.g mise jour du guide, faire des posts de blog sur Plume)

Cercles actuels :

- Le conseil d'administration

- Infra → Alex et Quentin référents "de fait". On n'a plus d'employés à plein temps → il faut essayer de distribuer la charge de travail sur tout le monde, notamment pour le maintien de l'infra. Chantier à pousser : l'horizontalité de l'infra.

Cercles en réflexion :

- accueil : chan matrix pour coordonner les arrivées (e.g. quand on reçoit un mail sur coucou@deuxfleurs.fr)
- essaimage : on a écrit pas mal d'articles, il y a des volontés de continuer. Adrien motivé pour être référent du cercle
- stratégie : comment faire en sorte de pousser nos idées au sens large

**Recherche de fonds**

Pour l'instant les ex-salariés recherchent des financements de leur coté, pourrait-on envisager de refaire ça au niveau de l'asso ?

Cotisations et dons par virements bancaires → pour l'instant, suffisant pour maintenir la base de l'asso : coûts bancaires, frais de fonctionnement. Pas plus.

Envisager une plateforme de dons récurrents ? e.g. liberapay, opencollective, tipee, heloasso, utip, ko-fi. Si des gens ont des opinions sur ces plateformes, faites-nous en part.

Rechercher de nouvelles subventions publiques ?

**Poursuite de la maturité technique**

Garage a énormément progressé durant NGI. On voudrait continuer à construire sur cet élan, avec la communauté.

Au niveau infra, résoudre les soucis mentionnés précédemment pour que tout soit full redondé, que la coupure de fibre/d'élec ne pose littéralement aucun problème.

**Tisser des liens**

On a beaucoup parlé de l'aspect technique (s'extraire des datacenters, du modèle industriel). On a aussi des arguments politiques pour dire qu'il faut quitter les GAFAM. Comment faire le pont entre les deux et amener les gens à comprendre l'intérêt des chatons (ou du libre en général) et à les utiliser?

Intervention dans un lieu à Vaulx-en-Velin (Bricologis) pour faire des ateliers. Proposition de Quentin : acheter des PC portables pour montrer aux gens, leur permettre de faire des choses dessus (e.g. aider les gens à faire leurs démarches administratives). Proposition de budget : 300€.

2e proposition de Quention : faire une journée à la Cyberrance avec Club1 et Picasoft. Partir du truc des sites statiques, "poétiser le web", croiser la tech et l'art. Faire une journée un samedi de mai ou juin en région parisienne. Proposition de budget: 1000€. Être dans le constructif : faire une proposition d'informatique moins marchand, moins dans la commercialisation → "poétiser le web". La cyberrance = gens issus du monde de l'art, ça serait l'occasion de croiser des perspectives différentes. Très grand intérêt de la cyberrance et de club1 pour faire cette journée.

Rien n'est fixé pour tout ça encore. Phase de discussion à lancer.

# Votes et élections

**Vote 1.** Changement d'adresse de Rennes a Lille chez Adrien (on ne peut pas garder l'adresse à Rennes, Adrien compte lui rester à Lille à terme, et d'ailleurs aussi héberger des serveurs).

Nouvelle adresse: Adrien Luxey-Bitri
16 rue de la Convention, Appt. 1 RDC,
59800 Lille

8 voix pour, 0 voix contre. Adopté.

**Vote 2.** Maintenir le prix de la cotisation annuelle à l'association à 10€. (pas de nécessité de le changer, c'est juste que les statuts nous imposent de le voter en AG)

8 voix pour, 0 voix contre. Adopté.

**Élection du bureau.** Le dépouillement du scrutin à jugement majoritaire donne les trois candidats élus avec une mention médiane de Excellent : Adrien Luxey-Bitri, Vincent Giraud et Maximilien Richer. (unanimité d'excellent pour Maximilien).

# Questions et remarques supplémentaires

**Réflexion qualité de service.** Changement de vitesse (plus de salarié.e.s), mais on est toujours CHATONS avec des engagements. Mettre nos propres sites et nos mails sur l'infra Deuxfleurs (pour ceux qui l'ont pas fait) pour qu'on se motive plus à garantir son bon fonctionnement. → eat your own dogfood.

Ne pas non plus se mettre trop de pression sur la qualité, penser plutôt qu'on se soucie des autres. On est bénévoles, on ne veut pas se tuer à la tâche, mais on veut avoir une attention aux usager.e.s qui n'ont pas accès aux infra et n'ont pas les moyens de réparer ou changer une config. Avoir un sens des responsabilités, pour éviter une approche féodale/gatekeeping.

On met en place cet infra pour des raisons/des valeurs, ça serait bête d'utiliser autre chose (e.g. gandi/ovh/...)

**Camp deuxfleurs de l'été 2023?** Vincent aimerait se charger de l'orga d'un camp quand il aura plus de temps. Garder en tête qu'on est en recherche de lieu pour l'été 2023. C'est peut-être le dernier où on aura suffisamment de fonds pour couvrir l'intégralité du séjour. Pour trouver des idées de lieu, regarder sur le forum CHATONS : à chaque camp ils identifient plusieurs lieux, au total ça fait beaucoup de possibilités. Regarder quels sont leurs réseaux, de fil en aiguille on peut trouver des choses. Ne pas exclure de retourner au Couvent des Clarisses.
Anciela discute régulièrement avec Esther et Quentin au sujet de l'asso, a peut-être aussi quelques pistes.

À la montagne, planter la tente ? Beaucoup de possibilités.

**Réunions prévues prochainement**. Tous les 1ers mardis du mois, réunion mensuelle au coin du feu. Réunions techniques planifiées petit à petit.

**Calendrier partagé CalDav sur SoGo**

_Authenticated User Access_ (on voit tout et on peut tout modifier si on a un compte deuxfleurs)

- CalDAV URL <https://sogo.deuxfleurs.fr/SOGo/dav/lx/Calendar/22-63E29280-1-71341300/>
- WebDAV ICS URL <https://sogo.deuxfleurs.fr/SOGo/dav/lx/Calendar/22-63E29280-1-71341300.ics>
- WebDAV XML URL <https://sogo.deuxfleurs.fr/SOGo/dav/lx/Calendar/22-63E29280-1-71341300.xml>

_Public Access_ (on voit seulement les trucs publics)

- CalDAV URL <https://sogo.deuxfleurs.fr/SOGo/dav/public/lx/Calendar/22-63E29280-1-71341300/>
- WebDAV ICS URL <https://sogo.deuxfleurs.fr/SOGo/dav/public/lx/Calendar/22-63E29280-1-71341300.ics>
- WebDAV XML URL <https://sogo.deuxfleurs.fr/SOGo/dav/public/lx/Calendar/22-63E29280-1-71341300.xml>

*À faire :* mettre l'adresse sur le guide.

*À faire :* mise à jour automatique du calendrier sur la page d'accueil de Deuxfleurs ?

**Appel à participation au CA** On peut être jusqu'à 6, n'hésitez pas a vous impliquer. Charge de travail relativement faible, surtout depuis la fin de NGI. Lire les mails, tâches de compta/trésorerie, tenue de l'AG, organisation des camps, parfois gestion des conflits. Être responsable moralement de l'association. Pour des personnes voulant s'impliquer, ça peut être une bonne façon de découvrir le fonctionnement de l'asso, bon point de vue pour lancer des projets, etc. Et même les personnes extérieures au CA formellement peuvent prendre une part dans tout ça pour alléger la charge du CA. Pas de tâche foncièrement dévolue au CA où les autres ne peuvent pas s'impliquer, rien n'est interdit.
