---
title: "Nous rejoindre"
description:  "Comment rejoindre l'association"
weight: 1
extra:
  parent: 'vie_associative/_index.md'
---

Notre association fait la différence entre <a href="#usager-es">les usager·es</a> et <a href="#membres">les membres</a>.
Les premiers ne font que bénéficier de notre offre de services, mais ne participent pas à la vie de l'association. 
Les seconds, en plus d'utiliser nos services, participent aux discussions, décisions, et directions prises par l'association, notamment _via_ leur droit de vote en assemblée générale (AG).

# Usager·es

> **L'usage de nos services est conditionné au respect de <a href="/vie_associative/cgu/" target="_black">nos conditions générales d'utilisation (CGU)</a>.** Les lire et les accepter est un préalable à toute demande.

Vous êtes un particulier ou un collectif, et souhaitez utiliser <a href="/prise_en_main/" target="_blank">nos services</a>. 
Certains sont en accès libre (à date d'écriture : la suite bureautique et la visioconférence). 
Pour les autres (conversation instantanée , e-mail et site statique), il vous faudra nous envoyer une demande.
Deuxfleurs ne s'engage pas à fournir de support ou de développement spécifique, ni ne permet l'hébergement d'infrastructure matérielle ou logicielle — seulement un accès à des outils techniques.

* **Nous contacter :** Par e-mail à l'adresse `coucou[@]deuxfleurs.fr`, en discussion directe sur <a href="https://matrix.to/#/#deuxfleurs:deuxfleurs.fr" target="_blank">notre salon Matrix `#deuxfleurs:deuxfleurs.fr`</a> ou directement _via_ un membre (voir <a href="https://deuxfleurs.fr/#connaissance" target="_blank"> notre agenda</a>).

* **Prix :** Libre et conscient, conseillé à 15€/an.

  Trois moyens de paiement sont disponibles :

  - Via [HelloAsso](https://www.helloasso.com/associations/deuxfleurs/adhesions/prix-libre-compte-utilisateur-deuxfleurs), de préférence.
  - Par virement bancaire (vous pouvez demander notre RIB à l'adresse `ca@deuxfleurs.fr`).
  Le libellé du virement doit être de la forme : `Cotisation usage <nom ou pseudo> <année>`.
  - Via un membre, en espèces, en présentiel.

Nous estimons que si les usager·es payent 15€/an, iels remboursent leur occupation de notre infrastructure. _Payez ce que vous pouvez._ Personne ne vous en voudra si vous n'avez pas la capacité financière de contribuer. À l'inverse, si vous donnez plus, vous participez à l'hébergement des moins fortuné·es, et à notre viabilité économique (merci !).

  > À l'avenir, nous souhaiterions lancer un appel annuel à cotisation, détaillant nos recettes et nos dépenses, pour aligner le prix conseillé sur nos objectifs annuels. En attendant, nos rapports financiers sont accessibles dans <a href="/vie_associative/ag/" target="_blank">nos comptes-rendus d'AG</a>, et le prix conseillé est fixe.
  > 
  > _Message du conseil d'administration :_ Amateur·ices de comptabilité, rejoignez-nous ! 

* **Admission :** Automatique sauf dans les cas suivants :

  * La personne faisant la demande ne semble pas partager nos valeurs ;
  * La demande concerne un grand collectif (entreprise, association ou autre), en raison de votre usage potentiellement plus important de nos ressources.

  Dans ces cas-là, votre admission sera discutée collectivement (au plus tard lors de la réunion mensuelle de l'association), et la décision sera prise au consentement (qui ne dit explicitement « non » consent).

  > **A noter pour les membres :** *Vous prenez la responsabilité des personnes que vous invitez sur notre infrastructure.* Tout « doute raisonnable » de votre part doit mener à une discussion collective avant acceptation.

* **Rupture de l'accord :** En cas de violation des <a href="/vie_associative/cgu/" target="_black">CGU</a>.

# Membres

Vous souhaitez rejoindre l'association : participer à nos créations et interventions ; réfléchir à nos objectifs et idéaux ; discuter notre gouvernance et notre stratégie. 
Chic ! 
Nous serons toujours avides d'ajouter une voix à notre quorum, une main à notre patte et une critique à nos idées.
**Commencez donc par lire et accepter <a href="/vie_associative/statuts/" target="_blank">nos statuts</a>.**

La prise de contact s'effectue de la même manière que pour les usager·es. 
Néanmoins, nous nous laisserons un temps de réflexion avant de valider (ou non) votre demande. 
Une fois membre, vous serez invité·e à participer à tous nos débats et rencontres, disposerez du droit de vote en AG, et aurez la responsabilité de parler au nom de Deuxfleurs (_ouh, spooky_).

* **Nous contacter :** Par e-mail à l'adresse `coucou[@]deuxfleurs.fr`, en discussion directe sur <a href="https://matrix.to/#/#deuxfleurs:deuxfleurs.fr" target="_blank">notre salon Matrix `#deuxfleurs:deuxfleurs.fr`</a> ou directement _via_ un membre (voir <a href="https://deuxfleurs.fr/#connaissance" target="_blank"> notre agenda</a>).

* **Prix :** Fixe, de 10€/an.

  Trois moyens de paiement sont disponibles :

  - Via [HelloAsso](https://www.helloasso.com/associations/deuxfleurs), de préférence.
  - Par virement bancaire (vous pouvez demander notre RIB à l'adresse `ca@deuxfleurs.fr`).
  Le libellé du virement doit être de la forme : `Cotisation membre <nom ou pseudo> <année>`.
  - Via un membre, en espèces, en présentiel.

* **Admission :** Par _cooptation_. Tous les membres de l'association sont informés de votre demande, et peuvent formuler une objection.

  * **Sauf objection, le statut de membre sera accordé après deux semaines et après paiement de la cotisation**.
  * Si objection, votre admission sera discutée collectivement (au plus tard lors de la réunion mensuelle de l'association), et la décision sera prise au consensus (chaque membre doit explicitement dire « oui » pour que votre demande soit acceptée).

* **Perte de la qualité de membre :** Voir <a href="/vie_associative/statuts/#article-7-perte-de-la-qualite-de-membre" target="_blank">article 7 de nos statuts</a>.

> Nous n'avons pas encore rédigé de code de conduite pour les membres. Voilà néanmoins quelques points consensuels :
>
> * l'impératif du respect et de la tolérance ; 
> * préférer agir à plusieurs, notamment sur des opérations techniques (pour partager responsabilités et connaissances) ;
> * raconter _a posteriori_ quand vous parlez au nom de Deuxfleurs (pour  notamment s'assurer de l'alignement de nos valeurs).

---------

<a href="https://pad.deuxfleurs.fr/pad/#/2/pad/view/d4whN7WSzObRabvWq3gV6AcUGKoqR280rMIksLnXWQc/" target="_blank">Notes détaillées de la réunion du 06/10/22</a> ayant menée à la séparation des deux statuts d'usager·es et de membre.
