---
title: "Statuts"
description:  "Statuts de l'association"
weight: 20
extra:
  parent: 'vie_associative/_index.md'
---

# Article 1. Constitution et dénomination

Il est fondé entre les adhérents aux présents statuts une association
régie par la loi 1901, ayant pour titre Deuxfleurs.

# Article 2. Buts

Cette association a pour but de défendre et promouvoir les libertés
individuelles et collectives à travers la mise en place d'infrastuctures
numériques libres.

# Article 3. Siège social

Le siège social est fixé 
Chez Adrien Luxey-Bitri, 
RDC - Appt. 1, 
16 rue de la Convention, 
59800 Lille. 
Il pourra être transféré suite à un vote par l'assemblée générale.

# Article 4. Durée de l'association

L'association perdure tant qu'elle possède au moins un membre, ou
jusqu'à sa dissolution décidée en assemblée générale.

# Article 5. Admission et adhésion

Pour faire partie de l'association, il faut être coopté par un membre de
l'association, adhérer aux présents statuts et s'acquitter de la
cotisation annuelle dont le montant est de 10 euros.

# Article 6. Composition de l'association

L'association se compose exclusivement de membres admis selon les dispositions
de l'Article 5 et à jour de leur cotisation. Tout membre
actif possède une voix lors des votes en assemblée générale. Est considéré
actif tout membre présent à l'assemblée générale (physiquement, par
visioconférence ou par procuration écrite donnée à un autre membre de
l'association).

# Article 7. Perte de la qualité de membre

La qualité de membre se perd par :

- la démission,
- le non-renouvelement de la cotisation dans un délai de deux mois
  après le 1er Janvier de l'année courante,
- le décès,
- la radiation prononcée aux deux tiers des votes exprimés, lors d'un
  vote extraordinaire ou de l'assemblée générale.

# Article 8. L'assemblée générale

L'assemblée générale ordinaire se réunit au moins une fois par an, convoquée
par le conseil d'administration. L'assemblée générale extraordinaire est
convoquée par le conseil d'administration, à la demande de celui-ci ou à la
demande du quart au moins des membres de l'association.

L'assemblée générale (ordinaire ou extraordinaire) comprend tous les
membres de l'association à jour de leur cotisation. Quinze jours au
moins avant la date fixée, les membres de l'association sont convoqués
via la liste de diffusion de l'association et l'ordre du jour est
inscrit sur les convocations.

Le conseil d'administration anime l'assemblée générale. L'assemblée
générale, après avoir délibéré, se prononce sur le rapport moral et/ou
d'activités. Le conseil d'administration rend compte de l'exercice
financier clos et soumet le bilan de l'exercice clos à l'approbation de
l'assemblée dans un délai de six mois après la clôture des comptes.
L'assemblée générale délibère sur les orientations à venir et se
prononce sur le budget prévisionnel de l'année en cours.

Elle pourvoit, au scrutin secret, à la nomination ou au renouvellement
des membres du conseil d'administration via un scrutin au jugement majoritaire. Elle fixe le montant de la cotisation annuelle. Les décisions
de l'assemblée sont prises à la majorité des membres présents ou
représentés. Chaque membre présent ne peut détenir plus d'une
procuration.

# Article 9. Membres mineurs

Les mineurs peuvent adhérer à l'association sous réserve d'un accord
tacite ou d'une autorisation écrite de leurs parents ou tuteurs légaux.
Ils sont membres à part entière de l'association. Seuls les membres âgés
de 16 ans au moins au jour d'une élection sont autorisés à y voter,
notamment au cours d'une assemblée générale. Pour les autres, leur droit
de vote est transmis à leur représentant légal.

# Article 10. Le conseil d'administration

L'association est administrée par un conseil d'administration composé de
3 à 6 membres, élus pour 1 an dans les conditions fixées à
l'Article 8. Tous les membres de l'association à jour de
leur cotisation sont éligibles. En cas de vacance de poste, le conseil
d'administration peut pourvoir provisoirement au remplacement de ses
membres. Ce remplacement est obligatoire quand le conseil
d'administration compte moins de 3 membres. Il est procédé à leur
remplacement définitif à la plus prochaine assemblée générale. Les
pouvoirs des membres ainsi élus prennent fin à l'époque où devrait
normalement expirer le mandat des membres remplacés.

Le conseil d'administration met en œuvre les décisions de l'assemblée
générale, organise et anime la vie de l'association, dans le cadre fixé
par les statuts. Chacun de ses membres peut être habilité par le conseil
à remplir toutes les formalités de déclaration et de publication
prescrites par la législation et tout autre acte nécessaire au
fonctionnement de l'association et décidé par le conseil
d'administration. Tous les membres du conseil d'administration sont
responsables des engagements contractés par l'association. Tout contrat
ou convention passé entre l'association d'une part, et un membre du
conseil d'administration, son conjoint ou un proche, d'autre part, est
soumis pour autorisation au conseil d'administration et présenté pour
information à la plus prochaine assemblée générale. Le conseil
d'administration se réunit au moins 4 fois par an et toutes les fois
qu'il est convoqué par le tiers de ses membres. La présence de la moitié
au moins des membres du conseil est nécessaire pour que le conseil
d'administration puisse délibérer valablement. Les décisions sont prises
au consensus et, à défaut, à la majorité des voix des présents. Le vote
par procuration n'est pas autorisé.

# Article 11. Modification des statuts de l'association

Sur demande d'un tiers des membres actifs, ou sur demande du conseil
d'administration, des amendements aux statuts de l'association peuvent
être discutés et soumis au vote lors d'une assemblée générale, selon les
modalités de l'Article 8.
